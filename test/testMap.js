const items = [1,2,3,4,5,5];

const map = require("../map");

const callBackFunction=(value,index,items)=>{
    return value*index;
}

const array = map(items,callBackFunction);
if(array.length ===0){
    console.log("Undefined");
}else{
    console.log(array);
}
