function filter (elements, cb){
    let array = [];
    if(elements && cb){
        for(let i=0;i < elements.length ;i++){
            let check = cb (elements[i]);
            if(check){
                array.push(elements[i]);
            }
        }
    }
    return array;
}
module.exports = filter;