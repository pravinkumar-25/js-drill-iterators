function find(elements,cb){
    let value;
    if(elements && cb){
        for(let i=0;i < elements.length ;i++){
            let check = cb(elements[i]);
            if(check){
                return elements[i];
            }
        }
    }
    return value;
}
module.exports = find;