function reduce(elements, cb, startingValue){
    let value;
    if(elements && cb){
        let start =0;
        if(!startingValue){
            startingValue = elements[0];
            start =1;
        }
        value = startingValue;
        for(let i=start;i< elements.length;i++){
            value = cb(value,elements[i],i,elements);
        }
    }
    return value;
}
module.exports = reduce;