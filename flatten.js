function flatten (elements){
    let nestedArray =[];
    if(elements && Array.isArray(elements)){
        for(let i=0;i< elements.length;i++){
            if(Array.isArray(elements[i])){
                nestedArray = nestedArray.concat(flatten(elements[i]));
            }else{
                nestedArray.push(elements[i]);
            }
        }
    }
    return nestedArray;
}
module.exports = flatten;