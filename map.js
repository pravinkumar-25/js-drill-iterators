function map(elements, cb){
    var returnArray=[];
    if(elements && cb && Array.isArray(elements)){
        for(let i=0;i< elements.length;i++){
            let temp = cb(elements[i],i,elements);
            returnArray.push(temp);
        }
        
    }
    return returnArray;
}
module.exports = map;